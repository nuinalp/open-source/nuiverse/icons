# Nuiverse Design Icons
Nuiverse design icons is the official icon set of Nuinalp. The icons are designed under the Nuiverse Design Guidelines.

Visit our [website](https://nuinalp.gitlab.io/nuiverse/website-nv-icons/) and check all icons availible. We are working in a new website for a best experience to find and see the icons, available soon.

## Instalation

### Vanilla HTML

Easiest way to start using Nuiverse UI Icons is by adding a script tag to the CDN:

```html
<link href="https://cdn.jsdelivr.net/npm/@nuiverse/icons@latest/nv-icons.css" rel="stylesheet">
```
### Install in project via NPM

```bash
$ npm install @nuiverse/core@latest
```

and include the file in your html page.

```html
<link href="node_modules/@nuiverse/icons/nv-icons.css" rel="stylesheet">
```

## Usage

Use the `<i>` tag to represent an icon.

```html
<i class="nv-icons">camera</i>
```

## Contributions

If you wish to make changes to the icon font you'll need to follow these steps:

1. Add, remove or change respective .svg files inside images/.
2. Run $ npm install to get pull in all the required build tools.
3. Make sure you have fontforge and ttfautohint installed on your machine. The grunt-webfont installation guide outlines the prerequisites.
4. Run $ grunt.
5. Load index.html locally in your browser and check your icon looks good.
6. Submit a pull request.
7. Module owner will review, land, and stamp a new version.

## Guidelines

For best results, it is recommended to follow these guidelines:

- Make the document 30px × 30px (In Inkscape: File > Document Properties... > Custom size).
- Make the icon 24px × 24px.
- Center the icon (In Inkscape: Object > Align and Distribute... > Align relative to page).
- Make sure to have only one `<path>` with no overlap per icon.
- Optimise the icons using svgo, then export to plain SVG file ($ inkscape -l icon.svg icon.svg).

Please also make sure new icons naming is consistent with existing ones:

- Use lower case only.
- Separate words with hyphens.
- Use meaningful words rather than acronyms (e.g. top-left-crop-corner instead of t-l-crop-corner).

## License

[BSD Clause 3](LICENSE.md)

Copyright (c) 2019 The Nuinalp Authors. All rights reserved.<br>
Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
